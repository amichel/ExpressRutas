
var express = require('express');
var routes = express.Router();
/* GET home page. */
const home  = require('./home.js');
      cars  = require('./cars/cars.js');
      error = require('./error.js');
      users = require('./users/users.js');  

// Ruta 
routes.use('/', home);

// Ruta cars
routes.use('/cars', cars);

// Ruta usuarios
routes.use('/users',users);

// otras rutas
routes.use('*',error);



module.exports = routes;
