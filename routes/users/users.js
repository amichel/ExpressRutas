var   express = require('express');
var   users   = express.Router();
const usuario = require('../../controller/controllerUsers'); 

/* GET users listing. */
users.route('/')
      .get(usuario.getUsers)
      .post(usuario.createUser) 


module.exports = users;
