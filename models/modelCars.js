'use strict';

// Obtener el modelo es decir importar el modelo
// modelProductos donde se trae la bd y las tablas de los productos

// Create the model

// const r = require( 'rethinkdb' );
// const connect = require( '../public/config/connect' );

// exports.agregarProducto = ( request, response ) => {
//     let producto =  request.body;

//     r.db( 'catko' ).table( 'productos' )
//         .insert( producto ,{returnChanges: true} )
//         .run( request._rdb )
//         .then( cursor => cursor.toArray() )
//         .then( result => {
//             response.send(result);
//         } )
//         .catch( error => response.send( error ) );
// } ;

// exports.listarProductos = ( request, response ) => {
//     r.db( 'catko' ).table( 'productos' )
//         .run( request._rdb )
//         .then( cursor => cursor.toArray() )
//         .then( result => {
//             response.send( result );
//         } )
//         .catch( error => response.send( error ) );
// };

// exports.listarUnProducto = function (request, response) {
//     let producto_id = request.params.producto_id;
    
//         r.db( 'catko' ).table( 'productos' )
//             .get( producto_id )
//             .run( request._rdb )
//             .then( cursor => cursor.toArray() )
//             .then( result => {
//                 response.send( result );
//             } )
//             .catch( error => response.send( error ) );
// };


// exports.editarProducto = ( request, response ) => {
//     let producto_id = request.params.producto_id;

//     r.db( 'catko' ).table( 'productos' )
//         .get( producto_id )
//         .update( {
//             'email': request.body.email,
//             'name': request.body.name
//         } )
//         .run( request._rdb )
//         .then( cursor => cursor.toArray() )
//         .then( result => {
//             response.send( result );
//         } )
//         .catch( error => response.send( error ) );
// };

// exports.eliminarProducto = ( request, response ) => {
//     let producto_id = request.params.producto_id;

//     r.db( 'catko' ).table( 'productos' )
//         .get( producto_id )
//         .delete()
//         .run( request._rdb )
//         .then( cursor => cursor.toArray() )
//         .then( result => {
//             response.send( result );
//         } )
//         .catch( error => response.send( error ) );
// };